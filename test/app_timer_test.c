/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This document contains proprietary information belonging to Achuthan
    Paramanathan. Passing on and copying of this document, use and communication
    of its contents is not permitted without prior written authorisation.
    ============================================================================
    Revision Information:
        File name: app_uart_test.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "tests.h"
#include "timer/time.h"


/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */



/*==============================================================================
 ** Function...: app_timer_test
 ** Return.....: void
 ** Description: Testing the basic functionalities of the timer
 ** Created....: 23.07.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void app_timer_test(void)
{
    char s[100];

    unsigned int i = 0;
    LED_BLUE_TOGGLE;
    
    while(1)
    {
        delay_ms(1000);
        sprintf(s,"Seconds %i\n", i);
        uart_print(s);
        LED_GREEN_TOGGLE;
        LED_BLUE_TOGGLE;
        LED_RED_TOGGLE;
        i++;
    }
}