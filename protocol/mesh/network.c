/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: network.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include "globals/globals.h"
#include "network.h"


/*
 ** ============================================================================
 **                   LOCAL  FUNCTION DECLARATIONS
 ** ============================================================================
 */

GLOB_RET network_outgoing( uint8_t addr, uint8_t len, void *frame );
GLOB_RET network_incoming(uint8_t len, void *frame );
uint8_t findNextHop(uint8_t dest);

/*
** ==========================================================================
**                       Layer specific variables
** ==========================================================================
*/

nlme Nlme;


/*
** =============================================================================
**                    FUNCTIONS
** =============================================================================
*/

/*==============================================================================
** Function...: networklayerInit
** Return.....: GLOB_RET
** Description: network layer initialization
** Created....: 29.12.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void networklayerInit(void)
{
    Nlme.ttl = 7;
    Nlme.headerSize = 4;
    Nlme.networkSize = 20;
    Nlme.conBits = 0x00;

}

/*==============================================================================
** Function...: network_layer
** Return.....: GLOB_RET
** Description: network layer interface. All call must go thoug this interface
** Created....: 04.04.2013 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET network_interface(char iKey, uint8_t addr, uint8_t len, void *frame)
{
    GLOB_RET ret = GLOB_SUCCESS;

    if(OUTGOING==iKey)
    {
        ret = network_outgoing(addr, len, frame); 
    }
    else if (INCOMING == iKey)
    {
        ret = network_incoming(len, frame);
    }
    else
    {
        ret = GLOB_ERROR_INVALID_PARAM;
    }
    return ret;
}


/*==============================================================================
** Function...: network_outgoing
** Return.....: GLOB_RET
** Description: private function that handles all outgoing packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET network_outgoing( uint8_t addr, uint8_t len, void *frame )
{
    GLOB_RET ret = GLOB_SUCCESS;
    uint8_t  rsv = 0, nextDest = 0;
    uint8_t netFrame[DATA_SIZE+4] = {0};
    uint8_t netFrameSize = len + Nlme.headerSize;
    
    memcpy(netFrame, &Nlme.ttl, 1); // TTL
    memcpy(netFrame+1, &addr, 1);   // mesh dest
    memcpy(netFrame+2, &Nlme.conBits, 1);    // con
    memcpy(netFrame+3, &rsv, 1);    // rsv
    
    memcpy(netFrame+4, frame, len);
    
    //hexdump(1, "----- NET LAYER OUT----", netFrame, 4);
    
    // Add MAC / MESH header
    nextDest = findNextHop(addr);
    mac_interface(OUTGOING, nextDest,  netFrameSize, netFrame);
    
    return ret;
}


/*==============================================================================
** Function...: network_incoming
** Return.....: GLOB_RET
** Description: private function that handles all incomming packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET network_incoming(uint8_t len, void *frame)
{
    GLOB_RET ret = GLOB_SUCCESS;
    uint8_t data[24] = {0};
    uint8_t netFrame[4] = {0};
    
    memcpy(netFrame, frame, 4);
    hexdump(1, "----- NET LAYER IN----", netFrame, Nlme.headerSize);
    memcpy(data, frame+Nlme.headerSize, 24);
    
    if (! buffer_full(&rxBuf))
    {
        write_buffer(data, &rxBuf);
    }
    else
    {
        uart_print("Buffer full\n");
    }
    
    return ret;
}


/*==============================================================================
** Function...: findNextHop
** Return.....: uint8_t
** Description: private function that find the next hop.
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t findNextHop(uint8_t dest)
{
    uint8_t nextHop = 0x00;
    
    if (255 == dest)
    {
        nextHop = dest;
    }
    
    return nextHop;
}