/*  =============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    =============================================================================
    This document contains proprietary information belonging to Achuthan Paramanathan.
    Passing on and copying of this document, use and communication of its contents 
    is not permitted without prior written authorisation.
    =============================================================================
    Revision Information:
        File name: kernel.h
        Version:   v0.0
        Date:      21-05-2015
    =============================================================================
*/


/*
** ==============================================================================
**                        INCLUDE STATEMENTS
** ==============================================================================
*/

#ifndef OS_H
#define OS_H

#include <stdint.h>
#include <stdbool.h>

#include "terminal/terminal.h"
#include "uart/uart.h"

/*
** =============================================================================
**                   GLOBAL EXPORTED FUNCTION DECLARATIONS
** =============================================================================
*/

void os(void);
int send_msg(int receiver, int prio, uint8_t *msg, int lgt);
void start_tasks(void);

/*
** =============================================================================
**                                   DEFINES
** =============================================================================
*/

#define RING_BUF_SIZE 64
#define NR_PRIO 5
#define NR_INTR 7
#define LOCK lock  /* disable_interrupt(); */
#define UNLOCK unlock /* enable_interrupt(); */

/*
** ==========================================================================
**                       Extern Global variables
** ==========================================================================
*/



#endif //OS_H