/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: os.c
        Version:   v0.0
        Date:      21-05-2015
    ============================================================================
 */


/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */
#include "os.h"


/*
 ** ============================================================================
 **                           LOCAL DEFINES
 ** ============================================================================
 */


/* 
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */

/*
 ** ============================================================================
 **                       LOCAL VARIABLES 
 ** ============================================================================
 */

unsigned int prevSeq = 0, prevSeq_b = 0;
unsigned long pktcnt = 0, pktcnt_b = 0;
unsigned long pktloss = 0, pktloss_b = 0;


typedef struct {
    int receiver, sender;
    int lgt;
} msg_hd_tp;


uint8_t buf[NR_PRIO][RING_BUF_SIZE];
int r[NR_PRIO], w[NR_PRIO], cnt[NR_PRIO];

int running = -1; /* who is running ?! */
int nr_task = 0;

/* msg_send
 *
 * Can be used as tasks as well as ISRs because interrupt is
 * disabled so no hazard will occur
 * NOTE: optimization can be done by
 * looking at all the copying of data - Data will be 3 places in
 * a send: (1. at sender, 2. at buffersystem, 3. at receivre)
 * 
 */
void lock (void) { }

void unlock (void) { }

int send_msg (int receiver, int prio, uint8_t *msg, int lgt)
{
    msg_hd_tp hdr;
    uint8_t * index;
    int i, dlgt, wi;

    /* is prio valid ? */
    if (NR_PRIO <= prio)
        return -1; /* prio out of range */

    /* from here we do not want to be interrupted of ...
     * yes - ISR 
     */
    LOCK ();

    /* check for space in ringbuf */
    dlgt = lgt + (int) sizeof (msg_hd_tp);
    if ((RING_BUF_SIZE - cnt[prio]) < dlgt)
        return -2; /* no space */

    hdr.receiver = receiver;
    hdr.sender = running;
    hdr.lgt = lgt;

    /* for fast access */
    wi = w[prio]; /* for fast access */
    index = &(buf[prio][0]);

    /* first copy header */
    for (i = 0; i< sizeof (msg_hd_tp); i++)
    {
        *(index + wi++) = *((uint8_t *) (&(hdr)) + i);
        if (RING_BUF_SIZE <= wi)
            wi = 0;
    }

    if (0 < lgt) /* is there any data ? */
    {

        /* copy data area */
        for (i = 0; i < lgt; i++)
        {
            *(index + wi++) = *(msg + i);
            if (RING_BUF_SIZE <= wi)
                wi = 0;
        }
    }

    /* adjust nr data in buf for the additional data */
    cnt[prio] += (int) sizeof (msg_hd_tp) + lgt;
    w[prio] = wi; /* back again */

    /* now lets open for the world again */
    UNLOCK ();
    return 0; /* standard ret code for ok */
}

/** In this function tasks are declared
 *
 */
int task_pool (int sender, int receiver, int priority, uint8_t *msg, int lgt)
{

    switch (receiver)
    {
        case 0:
        {

        }

        case 1:
        {
            terminal(msg);
            return 0; /* ok */
            break;
        }

        case 2:
            return 0; /* ok */
            break;

        case 3:
            return 0; /* ok */
            break;

        case 4:

            return 0; /* ok */
            break;
            
        default: 
            return 0; /* ok */
            break;
    }
}

int init_os (int nr_tasks)
{
    int i;
    nr_task = nr_tasks;

    for (i = 0; i < NR_PRIO; i++)
    {
        r[i] = w[i] = cnt[i] = 0;
    }
    return 0;
}


void start_tasks(void)
{
    send_msg (0, 3, (uint8_t *)"event", sizeof("event")); 
}


void an_ISR (void)
{
    LOCK ();
    /* do the HQ stuff you need 
     *  Now just send a msg to whom it may concerns ;-)
     */
    send_msg (1, 0, (uint8_t *)"tick", 4); /* always beware of lgt */
    UNLOCK ();
}

void os (void)
{
    static msg_hd_tp hdr;
    static int priority;
    static uint8_t data[RING_BUF_SIZE];
    int prio, i, ri, lgt;
    uint8_t * index;

    /* some init stuff */
    init_os (5);
    
    uart_print("OS initialized\n");
    
    start_tasks();
   


    /* now we are running */
    while (1)
    {
        /*find some one to run */
xxx:
        for (prio = 0; prio < NR_PRIO; prio++)
        {

            /* Silencio */
            LOCK ();
            if (cnt[prio])
            { /* pick message for sender */
                /* just for fast access */
                priority = prio;
                ri = r[prio]; /* for fast access */
                index = &(buf[prio][0]); /* ptr to start of msg buffer */

                /* first copy header */
                for (i = 0; i< sizeof (msg_hd_tp); i++)
                {
                    *(((uint8_t*) (&hdr)) + i) = *((uint8_t *) index + ri++);
                    if (RING_BUF_SIZE <= ri)
                        ri = 0;
                }

                /* next copy data area */
                lgt = hdr.lgt;
                for (i = 0; i < lgt; i++)
                {
                    data[i] = *((uint8_t *) index + ri++);
                    if (RING_BUF_SIZE <= ri)
                        ri = 0;
                }

                /* adjust nr data in buf for the additional data */
                cnt[prio] -= ((int) sizeof (msg_hd_tp) + lgt);
                r[prio] = ri;

                /* now do the schedule by calling the prs */
                running = hdr.receiver;

                /* Let the task run */
                UNLOCK ();
                task_pool (hdr.sender, running, priority, data, lgt);

                goto xxx; //break ; /* so go out int outer loop */
            }

            /* An unlock so ISRs can fight through 
             * There will be a LOCK just 20 lines ahead from here
             */
             UNLOCK ();
        }
    }
}