/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This document contains proprietary information belonging to Achuthan 
    Paramanathan. Passing on and copying of this document, use and communication 
    of its contents is not permitted without prior written authorisation.
    ============================================================================
    Revision Information:
        File name: terminal.c
        Version:   v0.0
        Date:      23-07-2015
    ============================================================================
 */

/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include <stdio.h>
#include <string.h>
#include "pps.h"
#include <uart.h>
#include <stdint.h>

#include "terminal.h"





/*
 ** ============================================================================
 **                           LOCAL DEFINES
 ** ============================================================================
 */
#define MAX_ARGV 40


/*
 ** ============================================================================
 **                   LOCAL EXPORTED FUNCTION DECLARATIONS
 ** ============================================================================
 */
uint16_t parseArgs(char *str, char *argv[]);
void menu(uint16_t argc, char *argv[]);
void help_command(uint16_t argc, char *argv[]);
void get_commands(uint16_t argc, char *argv[]);
uint8_t set_commands(uint16_t argc, char *argv[]);


/*
 ** ============================================================================
 **                       LOCAL VARIABLES 
 ** ============================================================================
 */
uint16_t argc;
char *argv[MAX_ARGV];

/*==============================================================================
 ** Function...: terminal
 ** Return.....: void 
 ** Description: Parse userinputs 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void terminal(void *msg) {
    char cmdStr[MAX_ARGV];
    memcpy(cmdStr, msg, MAX_ARGV);
    argc = parseArgs (&cmdStr[0], argv);
    menu(argc, argv);
}


/*==============================================================================
 ** Function...: parseArgs
 ** Return.....: void 
 ** Description: Parse userinputs 
 ** Created....: 21.05.2015 by Achuthan
 ** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint16_t parseArgs(char *str, char *argv[]) {
  uint16_t i = 0;
  char *ch = str;

  if (*ch == '\r') {
    return 0;
  }

  while (*ch != '\0') {
    i++;

    /*Check if length  exceeds*/
    if (i > MAX_ARGV) {
      uart_print("Too many arguments\n");
      return 0;
    }
    argv[i - 1] = ch;
    while (*ch != ' ' && *ch != '\0' && *ch != '\r' && *ch != '\n')
    {
      if (*ch == '"') // Allow space characters inside double quotes
      {
        ch++;
        argv[i - 1] = ch; // Drop the first double quote char
        while (*ch != '"') {
          if (*ch == '\0' || *ch == '\r') {
            uart_print("Syntax error\n");
            return 0;
          }
          ch++; // Record until next double quote char
        }
        break;
      } else {
        ch++;
      }
    }
    if (*ch == '\r') {
      break;
    }
    if (*ch != '\0') {
      *ch = '\0';
      ch++;
      while (*ch == ' ') {
        ch++;
      }
    }
  }
  return i;
}



/*==============================================================================
** Function...: menu
** Return.....: void
** Description: Terminal menu
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void menu(uint16_t argc, char *argv[])
{
    char s[200];
  
    if (argc) 
    {
        if (strcmp(argv[0], "help") == 0) 
        {
            help_command(argc, argv);
        }
        else if(strcmp(argv[0], "get") == 0)
        {
            get_commands(argc, argv);
        }
        else if(strcmp(argv[0], "set") == 0)
        {
            set_commands(argc, argv);
        }
        else 
        {
          sprintf(s, "Error: unknown cmd: %s\n", argv[0]);
          uart_print(s);
          uart_print("Type <help> for available commands");
        }
    }
}



/*==============================================================================
** Function...: help_command
** Return.....: void
** Description: print out the help coomands
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void help_command(uint16_t argc, char *argv[])
{
    
}


/*==============================================================================
** Function...: set_commands
** Return.....: void
** Description: set commands
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
uint8_t set_commands(uint16_t argc, char *argv[])
{
    return 0;
}


/*==============================================================================
** Function...: get_commands
** Return.....: void
** Description: get commands
** Created....: 25.03.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void get_commands(uint16_t argc, char *argv[])
{
 
}