/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: ringBuffer.h
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#include "globals/globals.h"
#include "uart/uart.h"

/*
** ==========================================================================
**                        DEFINES AND MACROS
** ==========================================================================
*/



/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

int buffer_full(rBufPar *bPar);
int buffer_empty(rBufPar *bPar);
void write_buffer( uint8_t *dPtr, rBufPar *bPar);
void read_buffer(uint8_t *dPtr, rBufPar *bPar);


