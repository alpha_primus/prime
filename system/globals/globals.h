 /*  =========================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: globals.h
        Version:   v1.0
        Date:      25-09-2017
    =========================================================================
*/


/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef GLOB_H
#define	GLOB_H

#include <string.h>
#include <stdint.h>          /* For uint32_t definition */
#include <stdio.h>
#include <stdlib.h> 


/*
** ==========================================================================
**                        DEFINES AND MACROS
** ==========================================================================
*/

#define BUFFER_SIZE 32
#define PACKET_SIZE 32
#define DATA_SIZE   24 
#define HEADER_SIZE  4




 /* ********************** TIMERs  *********************** */

#define TIMER_ONE_SEC 1000
#define TIMER_100_MS   100
#define TIMER_50_MS     50
#define TIMER_10_MS     10
#define TIMER_1_MS       1

#define START_ONE_MILI_TIMER  oneMilliTimer    = 0;
#define START_TIK_TIMER oneTikTimer = 0;

#define NO 0
#define YES 1

#define OUTGOING 1
#define INCOMING 0

#define RX 0
#define TX 1

/* ********************** LEDs *********************** */

#define LED_GREEN LATAbits.LATA0
#define LED_GREEN_TOGGLE LED_GREEN   = ~LED_GREEN

#define LED_BLUE LATAbits.LATA1
#define LED_BLUE_TOGGLE LED_BLUE = ~LED_BLUE

#define LED_RED LATAbits.LATA2
#define LED_RED_TOGGLE LED_RED = ~LED_RED


/**
 * Enumeration containing different return types
*/
typedef enum
{
  
  BUFFER_FULL                   = 10,  
  BUFFER_EMPTY                  = 9,  
    
  DATA_LINK_TRANSMISSION_FAILED = 8,  
  DATA_LINK_ACK_TIMEOUT         = 7, /* Payload sent but did not receive ack */
  DATA_LINK_GOT_ACK             = 6, /* Payload sent and received ack */
  DATA_LINK_CTS_TIMEOUT         = 5, /* RTS sent but did not receive cts*/
  DATA_LINK_GOT_CTS             = 4, /* RTS sent and received cts*/
  DATA_LINK_DIFS_WAIT_OK        = 3, /* Waited DIFS without any CD*/
  DATA_LINK_BACKOFF_OK          = 2, /* Did backoff without CD*/
  DATA_LINK_CARRIER_DETECTED    = 1, /* Carrier detected */

  GLOB_SUCCESS                  = 0,  /* The prosses was prossed with success */
  GLOB_FAILURE                  = -1, /* The prosses was prossed with some or more error */
  GLOB_ERROR_OUT_OF_RANGE_PARAM = -2, /* The parameter passed to the function is outside the valid range */
  GLOB_ERROR_INVALID_MESSAGE    = -3, /* The given message does not identify a valid object */
  GLOB_ERROR_INVALID_PARAM      = -4, /* The parameter passed to the function is invalid*/
  GLOB_ERROR_OUT_OF_HANDLES     = -5, /* There is no free handle available*/
  GLOB_ERROR_INIT               = -6, /* Initialisation when wrong */
  GLOB_WARNING_PARAM_NOT_SET    = -20, /* There requiret parameter is not set */
}GLOB_RET;


/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

void hexdump(int level, const char *title, const void *buf, char len);



/*
** ==========================================================================
**                        STRUCTS
** ==========================================================================
*/

typedef struct
{
    unsigned volatile int read_pointer;
    unsigned volatile int write_pointer;
    unsigned volatile int data_size;
    uint8_t buffer[BUFFER_SIZE][DATA_SIZE];
}rBufPar;


/*
 ** ==========================================================================
 **                       Extern Global variables
 ** ==========================================================================
 */

// External Timer vars
extern unsigned int oneMilliTimer;
extern unsigned int oneTikTimer;
extern unsigned long long longCounter_MS;

// External Buffer var
extern rBufPar txBuf, rxBuf;


#endif	/* GLOB_H */