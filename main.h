/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: main.h
        Version:   v1.0
        Date:      25-04-2015
    =========================================================================
*/


/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef MAIN_H
#define	MAIN_H

#include "globals/globals.h"

#include "uart/uart.h"
#include "timer/time.h"
#include "spi/spi.h"
#include "nrf905/nrf.h"

#include "application/com_transporter.h"
#include "test/tests.h"

#include "network.h"
#include "dataLink.h"


#endif	/* MAIN_H */