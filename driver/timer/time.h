/*  =========================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software; you can redistribute it and/or
    modify it under the terms of the GNU General Public License
    version 2 as published by the Free Software Foundation.
    ============================================================================
    Revision Information:
        File name: time.h
        Version:   v1.0
        Date:      25-04-2015
    =========================================================================
*/

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#include <p24Fxxxx.h>
#include <stdint.h>


/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/


#ifndef TIMER_H
#define	TIMER_H


typedef void (*cb_timer)(void);
void register_timer_cb(cb_timer ptr_reg_cb);
uint8_t timer2_cb(void);
void delay_ms(uint16_t t);


/**
 * Description: initializes the timer
 * @param: void
 * @return: global return code
*/
void timer(void);

/**
 * Description: timer interrupt
*/
void __attribute__((__interrupt__, auto_psv)) _T1Interrupt(void);


#endif	/* TIMER_H */
