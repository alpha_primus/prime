/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: timer.c
        Version:   v0.0
        Date:      21-05-2015
    ============================================================================
 */

/*============================================================================*/
/*                           INCLUDE STATEMENTS
==============================================================================*/

#include "time.h"

/*
** =============================================================================
**                           LOCAL VARIABLES
** =============================================================================
*/

// For Timer 1
unsigned int time_tik = 0;
unsigned int oneMilliTimer = 0;
unsigned int oneTikTimer = 0;
unsigned long long longCounter_MS = 0;
unsigned int difsTimer = 0;
unsigned int ackTimer = 0;
unsigned int slotTimer = 0;

// Delay timer used with Timer 1
unsigned int delayMS_t = 0;


static cb_timer ptr_reg_tx_cb;
static uint8_t tx_cb_flag = 0;


/*------------------------------------------------------------------------------
** Function...: timer
** Return.....: void
** Description: Initialize timer interrupt
** Created....: 09.01.2012 by Simon
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void timer(void)
{   
   PR1 = 0x64;
  
   T1CONbits.TSIDL = 0;  //Continue module operation in Idle mode
   T1CONbits.TGATE = 0;  //Gated time accumulation is disabled
   T1CONbits.TCKPS = 0b01; //scale 1:8
   T1CONbits.TSYNC = 0;    //Do not synchronize external clock input
   T1CONbits.TCS   = 0;  //Internal clock (FOSC/2)

   IPC0bits.T1IP   = 5;	 //set interrupt priority (Between 1 and 7)

   IFS0bits.T1IF   = 0;	 //reset interrupt flag
   IEC0bits.T1IE   = 1;	 //turn on the timer1 interrupt

   T1CONbits.TON   = 1;  //Start timer
   
   

   
   
   IPC1bits.T2IP   = 7;
   
   IFS0bits.T2IF   = 0;
   IEC0bits.T2IE   = 1;
   
   T2CONbits.TON   = 1;
}


/*------------------------------------------------------------------------------
** Function...: timer ISR
** Return.....: void
** Description: ISR
** Created....: 09.08.2012 by Achuthan
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void __attribute__((__interrupt__, auto_psv)) _T1Interrupt(void)
{
    
    time_tik++;
    oneTikTimer++;
    difsTimer++;
    slotTimer++;
    ackTimer++;
    
    if(oneTikTimer>60000)
    {
        oneTikTimer=0;
    }
    
    /* one millisecond */
    if(time_tik >= 10) 
    {
        time_tik = 0;
        oneMilliTimer++; // millisecond counter
        delayMS_t++; // millisecond counter for delay 
        longCounter_MS++; //long count milliseconds
        
    }
    
    IFS0bits.T1IF = 0;

}

/*------------------------------------------------------------------------------
** Function...: timer_2 ISR
** Return.....: void
** Description: ISR
** Created....: 04.01.2016 by Achuthan
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void __attribute__((__interrupt__, auto_psv)) _T2Interrupt(void)
{
    timer2_cb();
    IFS0bits.T2IF = 0; //Reset Timer2 interrupt flag and Return from ISR
}

/*------------------------------------------------------------------------------
** Function...: register_timer_tx_cb
** Return.....: void
** Description: registration of timer callback functions
** Created....: 06.01.2016 by Simon
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void register_timer_cb(cb_timer ptr_reg_cb)
{
    ptr_reg_tx_cb = ptr_reg_cb;
    tx_cb_flag = 1;                           
}


/*------------------------------------------------------------------------------
** Function...: timer_tx_cb
** Return.....: void
** Description: calling the tx cb function
** Created....: 06.01.2016 by Simon
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
uint8_t timer2_cb(void)
{
    uint8_t ret = 1;
    
    if(tx_cb_flag)
    {
        (*ptr_reg_tx_cb)();
        ret = 0;
    }
    
    return ret;
}


/*------------------------------------------------------------------------------
** Function...: delay_ms
** Return.....: void
** Description: millisecond delay
** Created....: 25.02.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
------------------------------------------------------------------------------*/
void delay_ms(uint16_t t)
{
    delayMS_t = 0;
    while(delayMS_t<t);
}


