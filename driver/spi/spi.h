/*  =========================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: spi.h
        Version:   v1.0
        Date:      25-04-2015
    =========================================================================
*/


/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef SPI_H
#define	SPI_H

#include <p24FJ64GB002.h>
#include <stdint.h>


/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

int spi(void);
uint8_t SPI_wr(uint8_t data); 

#endif	/* SPI_H */