/*  =============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    =============================================================================
    This document contains proprietary information belonging to Achuthan 
    Paramanathan. Passing on and copying of this document, use and communication 
    of its contents is not permitted without prior written authorisation.
    =============================================================================
    Revision Information:
        File name: uart.h
        Version:   v0.0
        Date:      21-05-2015
    =============================================================================
 */

#ifndef UART_H
#define	UART_H


/*
 ** ============================================================================
 **                        INCLUDE STATEMENTS
 ** ============================================================================
 */

#include <p24FJ64GB002.h>
#include <stdio.h>
#include <string.h> 
#include "pps.h" 
#include <uart.h> 
#include <stdint.h> 

#include "terminal/terminal.h"

void UART1Init(void);
void uart_print(char* ToBeSent);

#endif //UART_H