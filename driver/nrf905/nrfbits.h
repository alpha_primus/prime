/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: nrfbits.h
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

#ifndef NRFBITS_H
#define	NRFBITS_H

// Port config for nRF905 ( pin function)
#define TX_EN       LATAbits.LATA4    //TX_EN=1 TX mode, TX_EN=0 RX mode
#define TRX_CE      LATBbits.LATB14    //Enables chip for receive and transmit
#define PWR_UP      LATBbits.LATB13    //Power up chip

#define CD          PORTBbits.RB2     //Carrier Detect
#define AM          PORTBbits.RB3     //Address Match
#define DR          PORTBbits.RB4     //Receive and transmit Data Ready

#define CSN         LATBbits.LATB7    //SPI enable, active low






#endif	/* NRFBITS_H */

