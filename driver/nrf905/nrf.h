/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    All rights reserved.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: nrf.h
        Version:   v0.0
        Date:      2012-08-09
    ============================================================================
 */

/*
** ==========================================================================
**                        INCLUDE STATEMENTS
** ==========================================================================
*/

#ifndef NRF905_H
#define	NRF905_H

#include <p24FJ64GB002.h>
#include "nrfbits.h"
#include "nrfcfg.h"

#include "globals/globals.h"
#include "spi/spi.h"
#include "dataLink.h"


/*
** =============================================================================
**                       EXPORTED FUNCTION DECLARATION
** =============================================================================
*/

int TXPacket(void);
int nrf(void);
int nrfConfiguration(void);
void setNRFpw(int type, char pw);
void clockOut(void* data, int size);
void clockIn(void* data, int size);
int transmitFrame(void* data, char pw);

#endif	/* NRF905_H */