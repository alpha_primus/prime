/*  ============================================================================
    Copyright (C) 2015 Achuthan Paramanathan.
    ============================================================================
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
    ============================================================================
    Revision Information:
        File name: com_transport.c
        Version:   v0.0
        Date:      26-09-2017
    ============================================================================
 */


/*
** =============================================================================
**                        INCLUDE STATEMENTS
** =============================================================================
*/

#include "com_transporter.h"


/*
** =============================================================================
**                   LOCAL  FUNCTION DECLARATIONS
** =============================================================================
*/
void appInit(void);
void comTaskHandler(void);
void sendBeacon(void);
GLOB_RET com_outgoing(void);
GLOB_RET com_incomming(void);
void comTrans_interface(uint8_t comTask);
GLOB_RET inPacketHandler(void);


/*
 ** ============================================================================
 **                   LOCAL VARIABLES
 ** ============================================================================
 */
alme Alme;

uint8_t comTask = 0;
static uint16_t beaconIntervalCnt = 0; 


/*==============================================================================
** Function...: appInit
** Return.....: void
** Description: Application initialization
** Created....: 26.09.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void appInit(void)
{
    Alme.pktType = 0x0;
}


/*==============================================================================
** Function...: comTrans
** Return.....: void
** Description: Main application loop
** Created....: 26.09.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void comTrans(void)
{
    /* initialize function pointer to comTaskHandler */
    cb_timer ptr_comTaskHandler_cb = comTaskHandler;
    /* register the callback function */
    register_timer_cb(ptr_comTaskHandler_cb); 
    
    while(1)
    {
        comTrans_interface(comTask);
        comTask++;
        if(comTask>2) comTask = 0;
    }
}


/*==============================================================================
** Function...: comTrans_interface
** Return.....: void
** Description: All external calls must go through this interface
** Created....: 26.09.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void comTrans_interface(uint8_t comTask)
{
    GLOB_RET res;
    
    switch(comTask)
    {
        case INCOMING:
            res = com_incomming();
            break;

        case  OUTGOING:
            res = com_outgoing();
            break;

        default:
            break;
    }
}


/*==============================================================================
** Function...: com_incomming
** Return.....: GLOB_RET
** Description: private function that handles all incomming packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
GLOB_RET com_incomming(void)
{
    GLOB_RET res;

    if (!buffer_empty(&rxBuf))
    {
        res = inPacketHandler();
    }
    else
    {
        res = BUFFER_EMPTY;
    }
    
    return res;
    
}


/*==============================================================================
** Function...: com_outgoing
** Return.....: GLOB_RET
** Description: private function that handles all outgoing packets
** Created....: 01.05.2015 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/

GLOB_RET com_outgoing(void)
{
    GLOB_RET res;
    
    if (!buffer_empty(&txBuf))
    {
        uint8_t dest = 0x00;

        uint8_t data[DATA_SIZE] = {0};
        read_buffer(data, &txBuf);
        
        if(BEACON == data[0])
        {
            dest = 255;
            //hexdump(1, "----- APP LAYER OUT----", data, DATA_SIZE);
        }
        
        res = network_interface(OUTGOING, dest, DATA_SIZE, data);
    }
    else
    {
        res = BUFFER_EMPTY;
    }
    
    return res;
}


/*==============================================================================
** Function...: sendBeacon
** Return.....: void
** Description: Send a beacon very pre defined interval
** Created....: 26.09.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void sendBeacon(void)
{
    static uint16_t pktCnt;
    
    uint8_t bData[DATA_SIZE] = {0};
       
    bData[0] = BEACON;                
    memcpy(bData+1, &pktCnt, 2);
    
    if (! buffer_full(&txBuf))
    {
        write_buffer(bData, &txBuf);
    }
    else
    {
        uart_print("Buffer full\n");
    }
    
    pktCnt++;
}


/*==============================================================================
** Function...: comTaskHandler
** Return.....: void
** Description: Handles those time related tasks
** Created....: 26.09.2017 by Achuthan
** Modified...: dd.mm.yyyy by nn
==============================================================================*/
void comTaskHandler(void)
{
    beaconIntervalCnt++;
    
    if(beaconIntervalCnt>=650) // FIXME 650 = 5 sec
    {
        sendBeacon();
        beaconIntervalCnt = 0;
    }
}


GLOB_RET inPacketHandler(void)
{
    GLOB_RET res;
    
    uint8_t pktType = 0;
    uint8_t data[DATA_SIZE] = {0};
    read_buffer(data, &rxBuf);
    
    pktType = data[0];
    
    switch(pktType)
    {
        case BEACON:
            hexdump(1, "----- APP LAYER IN----", data, DATA_SIZE);
            break;

        case  OUTGOING:

            break;

        default:
            break;
    }
    
    return res;
}